package com.zero2oneit.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zero2oneit.mall.common.bean.member.MemberCoupon;
import com.zero2oneit.mall.common.query.member.MemberCouponQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-23
 */
public interface MemberCouponService extends IService<MemberCoupon> {

    /**
     * 加载会员优惠券列表
     * @param qo
     * @return
     */
    BoostrapDataGrid pageList(MemberCouponQueryObject qo);

    /**
     * 加载优惠券
     * @param qo
     * @return
     */
    R load(MemberCouponQueryObject qo);

    /**
     * 领取优惠券
     * @param qo
     * @return
     */
    R receive(MemberCouponQueryObject qo);

    /**
     * 更改优惠券状态
     * @param status
     */
    void status(String id, Integer status);

}

